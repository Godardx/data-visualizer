let dataArray = [];

// Fonction pour récupérer les données
get = function (url) {
    fetch(url)
        .then(response => response.json())
        .then(function (data) {
            all = data.people;
            for (let id = 0; id < all.length; id++) {
                let pos = all[id].contact.location.lon.toString() + " - " + +all[id].contact.location.lat.toString();
                dataArray.push({
                    id: all[id].id,
                    firstname: all[id].firstname,
                    lastname: all[id].lastname,
                    gender: all[id].gender,
                    email: all[id].contact.email,
                    address: all[id].contact.address,
                    city: all[id].contact.city,
                    country: all[id].contact.country,
                    pos,
                    phone: all[id].contact.phone,
                    pet: all[id].preferences.favorite_pet,
                    fruit: all[id].preferences.favorite_fruit,
                    color: all[id].preferences.favorite_color,
                    movie: all[id].preferences.favorite_movie
                });
            }
            createTable();
        });
};
// Récupération des données sur le site
get('https://run.mocky.io/v3/70e5b0ad-7112-41c5-853e-b382a39e65b7');

// Création du tableau
createTable = function () {
    table = new Tabulator("#people-table", {
        data: dataArray,
        layout: "fitColumns",
        responsiveLayout: "hide",
        tooltips: true,
        addRowPos: "top",
        history: true,
        pagination: "local",
        paginationSize: 20,
        movableColumns: false,
        resizableRows: true,
        initialSort: [
            { column: "id", dir: "asc" },
        ],
        // Tableau avec les données
        columns: [
            { title: "ID", field: "id", editor: "input", validator: "required", headerFilter: "input" },
            { title: "First Name", field: "firstname", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Last Name", field: "lastname", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Gender", field: "gender", width: 95, editor: "select", editorParams: { values: ["male", "female"] }, headerFilter: true, headerFilterParams: { "male": "Male", "female": "Female" } },
            { title: "E-Mail", field: "email", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Address", field: "address", editor: "input", validator: "required", headerFilter: "input" },
            { title: "City", field: "city", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Country", field: "country", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Position", field: "pos", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Phone", field: "phone", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Pet", field: "pet", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Fruit", field: "fruit", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Color", field: "color", editor: "input", validator: "required", headerFilter: "input" },
            { title: "Favorite Movie", field: "movie", editor: "input", validator: "required", headerFilter: "input" },
        ],
    });

    // Permet de télécharger le fichier JSON
    document.getElementById("download-json").addEventListener("click", function () {
        table.download("json", "people.json");
    });
};

